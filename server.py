#!/usr/bin/env python3

"""
	This file is part of ĞMixer-py.

	ĞMixer-py is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer-py is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer-py.  If not, see <https://www.gnu.org/licenses/>.
"""

import sys, os, json, asyncio, getpass, random, time, socket, secrets
from threading import Thread
import ubjson
import plyvel
import libnacl.sign
import socks
import duniterpy.api.bma as bma
from duniterpy.api.client import Client
from duniterpy.key import SigningKey, PublicKey
import utils

VERSION = "0.1.0"
AUTHORS = ["Pascal Engélibert <tuxmain@zettascript.org>"]

"""
Used terms:
sender_ = node which has sent something to me
receiver_ = node I'm sending something to
in_  = something that a node sent to me
out_ = something I'm sending to a node
"""

DIR = "~/.gmixer"
BIND_HOST = socket.gethostname()
try:
	BIND_HOST = socket.gethostbyname(BIND_HOST)
except socket.gaierror:
	pass
BIND_PORT = 10951
PUBLIC_HOST = BIND_HOST
PUBLIC_PORT = BIND_PORT
ID_SALT = ""
ID_PASSWORD = ""
BMA_HOSTS = ["g1.duniter.fr 443", "g1.duniter.org 443", "g1.presles.fr 443", "g1.cgeek.fr 443", "ts.g1.librelois.fr 443"]
MIX_INTERVAL = 60
MIX_MIN_TXS = 5 # minimum amount of txs to mix
MIX_REQ_AGE_MAX = 604800 # maximum mix request age before return to sender

def sendResponse(client, code, resp, dataformat="ubjson"):
	if dataformat == "ubjson":
		content_raw = ubjson.dumpb(resp)
		mime = "application/ubjson"
	elif dataformat == "json":
		try:
			content_raw = json.dumps(resp).encode()
		except TypeError:
			content_raw = json.dumps({"error": "non_ascii_resp"}).encode()
		mime = "text/json"
	client.sendall(("HTTP/1.1 "+code+"\r\nContent-type: "+mime+"; charset=UTF-8\r\nContent-length: "+str(len(content_raw))+"\r\n\r\n").encode()+content_raw)
	client.close()

class TX:
	def __init__(self, sender_pubkey=None, receiver_pubkey=None, onetime_pubkey=None, in_amount=None, in_base=None, out_amount=None, out_base=None, message=None, in_seeds=None, out_seeds=None, send_confirm=True, date=None, expire=None):
		self.sender_pubkey = sender_pubkey
		self.receiver_pubkey = receiver_pubkey
		self.onetime_pubkey = onetime_pubkey
		self.in_amount = in_amount
		self.in_base = in_base
		self.out_amount = out_amount
		self.out_base = out_base
		self.message = message
		self.in_seeds = in_seeds
		self.out_seeds = out_seeds
		self.send_confirm = send_confirm # True if sender is a server (in which case we send confirmation to it); False else (we wait a confirmation demand from it)
		self.date = date
		self.expire = expire
		
		self.need_send = True
		self.can_confirm = False
		self.need_confirm = True
		self.confirms = b""
		self.tx_sent = False
	
	def genMixConfirm(self, keys):
		message = {
			"document": "gmixer-mixconfirm1",
			"sender_pubkey": self.sender_pubkey,
			"receiver_pubkey": keys.pubkey,
			"in_amount": self.in_amount,
			"in_base": self.in_base,
			"in_seeds": self.in_seeds,
			"receiver_pubkey": self.receiver_pubkey,
			"out_amount": self.out_amount,
			"out_base": self.out_base,
			"out_seeds": self.out_seeds,
			"req_date": self.date,
			"expire_date": self.expire
		}
		message = ubjson.dumpb(message)
		message = keys.sign(message)
		message = PublicKey(self.onetime_pubkey).encrypt_seal(message)
		message = self.in_seeds[1] + self.in_seeds[2] + utils.int_to_bin(len(message)) + message + self.confirms
		message = PublicKey(self.sender_pubkey).encrypt_seal(message)
		message = keys.sign(message)
		return message
	
	def export_ubjson(self, db_txs):
		db_txs.put(self.in_seeds[2], ubjson.dumpb({
			"sender_pubkey": self.sender_pubkey,
			"receiver_pubkey": self.receiver_pubkey,
			"onetime_pubkey": self.onetime_pubkey,
			"in_amount": self.in_amount,
			"in_base": self.in_base,
			"out_amount": self.out_amount,
			"out_base": self.out_base,
			"message": self.message,
			"in_seeds": self.in_seeds,
			"out_seeds": self.out_seeds,
			"send_confirm": self.send_confirm,
			"date": self.date,
			"expire": self.expire,
			"need_send" : self.need_send,
			"can_confirm" : self.can_confirm,
			"need_confirm" : self.need_confirm,
			"confirms" : self.confirms,
			"tx_sent" : self.tx_sent,
		}))
	
	def import_ubjson(d):
		d = ubjson.loadb(d)
		tx = TX(d["sender_pubkey"], d["receiver_pubkey"], d["onetime_pubkey"], d["in_amount"], d["in_base"], d["out_amount"], d["out_base"], d["message"], d["in_seeds"], d["out_seeds"], d["send_confirm"], d["date"], d["expire"])
		tx.need_send = d["need_send"]
		tx.can_confirm = d["can_confirm"]
		tx.need_confirm = d["need_confirm"]
		tx.confirms = d["confirms"]
		tx.tx_sent = d["tx_sent"]
		return tx

def load_txs(db_txs, pool, tx_in_index, tx_out_index):
	for _, data in db_txs:
		tx = TX.import_ubjson(data)
		pool.append(tx)
		tx_in_index[tx.in_seeds[1]] = tx
		tx_out_index[tx.out_seeds[1]] = tx
	
	utils.logPrint("Loaded "+str(len(pool))+" txs", utils.LOG_TRACE)

def save_txs(db_txs, pool):
	for tx in pool:
		tx.export_ubjson(db_txs)

# Read ini config file
def readConfig(cdir, conf_overwrite={}):
	if not os.path.isfile(cdir+"/config.json"):
		configfile = open(cdir+"/config.json", "w")
		configfile.write("{}")
		configfile.close()
	
	with open(cdir+"/config.json", "r") as configfile:
		try:
			conf = json.load(configfile)
		except json.JSONDecodeError:
			utils.logPrint("Config: bad JSON => abort", utils.LOG_ERROR)
			exit(1)
	
	conf.setdefault("server", {})
	conf["server"].setdefault("bind_host", BIND_HOST)
	conf["server"].setdefault("bind_port", BIND_PORT)
	conf["server"].setdefault("public_host", PUBLIC_HOST)
	conf["server"].setdefault("public_port", PUBLIC_PORT)
	conf.setdefault("client", {})
	conf["client"].setdefault("bma_hosts", BMA_HOSTS)
	conf["client"].setdefault("proxy", None)
	conf["client"].setdefault("proxy_onion_only", False)
	conf.setdefault("crypto", {})
	conf["crypto"].setdefault("id_salt", ID_SALT)
	conf["crypto"].setdefault("id_password", ID_PASSWORD)
	conf.setdefault("mix", {})
	conf["mix"].setdefault("mix_interval", MIX_INTERVAL)
	conf["mix"].setdefault("mix_min_txs", MIX_MIN_TXS)
	conf["mix"].setdefault("mix_req_age_max", MIX_REQ_AGE_MAX)
	
	for key in conf_overwrite:
		c = conf
		k = key.split(".")
		for i in k[:len(k)-1]:
			c = conf[i]
		c[k[len(k)-1]] = conf_overwrite[key]
	
	with open(cdir+"/config.json", "w") as configfile:
		json.dump(conf, configfile, indent=1)
	
	return conf

class ServerThread(Thread):
	def __init__(self, conf, peers, peers_index, keys, pool, tx_in_index, tx_out_index, db_txs):
		Thread.__init__(self)
		
		self.conf = conf
		self.peers = peers
		self.peers_index = peers_index
		self.keys = keys
		self.pool = pool
		self.tx_in_index = tx_in_index
		self.tx_out_index = tx_out_index
		self.db_txs = db_txs
		
		self.sock = None
		self.work = True
		
	def run(self):
		server_addr = (self.conf["server"]["bind_host"], self.conf["server"]["bind_port"])
		if ":" in server_addr[0]: # IPv6
			self.sock = socket.socket(socket.AF_INET6, socket.SOCK_STREAM)
		else: # IPv4
			self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
		self.sock.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)
		self.sock.settimeout(5)
		self.sock.bind(server_addr)
		self.sock.listen(1)
		utils.logPrint("Server started at "+str(server_addr), utils.LOG_INFO)
		
		while self.work:
			try:
				client, addr = self.sock.accept()
			except socket.timeout:
				continue
			
			# Get request
			paquet = b""
			header = b""
			content = b""
			content_len = 0
			resp = {}
			lf = 0
			while True:
				raw = client.recv(utils.RECBUF)
				if raw:
					paquet += raw
					if lf >= 0:
						for c in raw:
							if c == 10:# LF
								lf += 1
							elif c != 13:# CR
								lf = 0
							if lf > 1:
								parts = paquet.split(b"\r\n\r\n")
								header = parts[0]
								content = parts[1]
								try:
									content_len = int(utils.p_clen.search(header.decode()).group(1))
								except (AttributeError, ValueError):
									content_len = 0
								break
						if lf > 1:
							break
				else:
					break
			while len(content) < content_len:
				raw = client.recv(utils.RECBUF)
				paquet += raw
				content += raw
			
			# Get URL
			httpreq = paquet.split(b"\n")
			try:
				url = httpreq[0].split(b" ")[1].decode().split("/")
			except IndexError:
				sendResponse(client, "400 Bad Request", {"error": "bad_http"})
				continue
			while "" in url:
				url.remove("")
			urll = len(url)
			
			resp = {}
			resp_format = "json" if "json" in url else "ubjson"
			
			# Treat request
			if "pubkey" in url:
				resp["pubkey"] = self.keys.pubkey
			
			if "version" in url:
				resp["version"] = VERSION
			
			if "mix" in url:
				sender_pubkey = utils.getargv("mix", "", 1, url)
				try:
					in_amount = int(utils.getargv("mix", "", 2, url))
					in_base = int(utils.getargv("mix", "", 3, url))
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_amount_base"}, resp_format)
					continue
				send_confirm = not "client" in url
				
				try:
					sender_keys = PublicKey(sender_pubkey)
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_sender_pubkey"}, resp_format)
					continue
				
				try:
					raw = libnacl.sign.Verifier(sender_keys.hex_pk()).verify(content) # Verify
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_signature"}, resp_format)
					continue
				
				try:
					data = self.keys.decrypt_seal(raw[32:]) # Decrypt
				except libnacl.CryptError:
					sendResponse(client, "403 Forbidden", {"error": "bad_encryption"}, resp_format)
					continue
				
				try:
					data = ubjson.loadb(data)
					assert "receiver" in data and type(data["receiver"]) == str
					assert "onetime" in data and type(data["onetime"]) == str
					assert "in_seeds" in data and type(data["in_seeds"]) == list and len(data["in_seeds"]) == 3 and type(data["in_seeds"][0]) == bytes and len(data["in_seeds"][0]) == 32 and data["in_seeds"][1] == None and data["in_seeds"][2] == None
					assert "out_seeds" in data and type(data["out_seeds"]) == list and len(data["out_seeds"]) == 3  and type(data["out_seeds"][0]) == bytes and len(data["out_seeds"][0]) == 32 and data["out_seeds"][1] == None
					assert "message" in data and type(data["message"]) == bytes
					assert (data["message"] == b"" and type(data["out_seeds"][2]) == bytes and len(data["out_seeds"][2]) == 32) or (data["message"] != b"" and data["out_seeds"][2] == None)
				except (ubjson.decoder.DecoderException, AssertionError):
					sendResponse(client, "403 Forbidden", {"error": "bad_ubjson"}, resp_format)
					continue
				
				receiver_pubkey = data["receiver"] # receiver pubkey
				onetime_pubkey = data["onetime"] # origin emitter one-time pubkey
				in_seeds = data["in_seeds"]
				out_seeds = data["out_seeds"]
				message = data["message"]
				
				in_seeds[1] = raw[:32]
				in_seeds[2] = secrets.token_bytes(32)
				out_seeds[1] = secrets.token_bytes(32)
				
				try:
					PublicKey(receiver_pubkey)
				except ValueError:
					sendResponse(client, "403 Forbidden", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				try:
					PublicKey(onetime_pubkey)
				except ValueError:
					sendResponse(client, "403 Forbidden", {"error": "bad_onetime_pubkey"}, resp_format)
					continue
				
				# Save tx in pool
				t = time.time()
				tx = TX(sender_pubkey, receiver_pubkey, onetime_pubkey, in_amount, in_base, in_amount, in_base, message, in_seeds, out_seeds, send_confirm, t, t+self.conf["mix"]["mix_req_age_max"])
				last_node = len(message) == 0
				tx.need_send = not last_node
				tx.can_confirm = last_node
				utils.logPrint("TX "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
				self.tx_out_index[out_seeds[1]] = tx
				self.tx_in_index[in_seeds[1]] = tx
				self.pool.append(tx)
				tx.export_ubjson(self.db_txs)
				
				resp["mix_ok"] = in_seeds[1]
			
			elif "confirm" in url:
				receiver_pubkey = utils.getargv("confirm", "", 1, url)
				try:
					out_seed1 = bytes.fromhex(utils.getargv("confirm", "", 2, url))
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
					continue
				
				if not out_seed1 in self.tx_out_index:
					sendResponse(client, "404 Not Found", {"error": "unknown_tx"}, resp_format)
					continue
				
				tx = self.tx_out_index[out_seed1]
				
				if len(tx.confirms) > 0 or tx.can_confirm or not tx.need_confirm or tx.need_send:
					sendResponse(client, "403 Forbidden", {"error": "cannot_confirm"}, resp_format)
					continue
				
				if receiver_pubkey != tx.receiver_pubkey:
					sendResponse(client, "401 Unauthorized", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				receiver_keys = PublicKey(tx.receiver_pubkey)
				
				try:
					data = libnacl.sign.Verifier(receiver_keys.hex_pk()).verify(content) # Verify
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_signature"}, resp_format)
					continue
				
				try:
					data = self.keys.decrypt_seal(data) # Decrypt
				except libnacl.CryptError:
					sendResponse(client, "403 Forbidden", {"error": "bad_encryption"}, resp_format)
					continue
				
				if data[:32] != tx.out_seeds[1] or len(data) < 64:
					sendResponse(client, "401 Unauthorized", {"error": "bad_seed"}, resp_format)
					continue
				
				tx.confirms = data[64:]# TODO check size
				tx.out_seeds[2] = data[32:64]
				
				utils.logPrint("Rec confirm "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
				tx.can_confirm = True
				
				resp["confirm_ok"] = tx.out_seeds[2]
			
			if "getconfirm" in url:
				sender_pubkey = utils.getargv("getconfirm", "", 1, url)
				try:
					in_seed1 = bytes.fromhex(utils.getargv("getconfirm", "", 2, url))
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_url"}, resp_format)
					continue
				
				if not in_seed1 in self.tx_in_index:
					sendResponse(client, "404 Not Found", {"error": "unknown_tx"}, resp_format)
					continue
				
				tx = self.tx_in_index[in_seed1]
				
				if not tx.can_confirm or not tx.need_confirm or tx.need_send:
					sendResponse(client, "403 Forbidden", {"error": "cannot_confirm"}, resp_format)
					continue
				
				if sender_pubkey != tx.sender_pubkey:
					sendResponse(client, "401 Unauthorized", {"error": "bad_rec_pubkey"}, resp_format)
					continue
				
				message = tx.genMixConfirm(self.keys)
				resp["confirm"] = message
				tx.need_confirm = False
				tx.export_ubjson(self.db_txs)
				utils.logPrint("Confirmed "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
			
			if "list" in url:
				peers_list = []
				for peer in self.peers:
					peers_list.append({"pubkey":peer.pubkey, "host":peer.host, "port":peer.port, "up":peer.up})
				resp["peers"] = peers_list
			
			if "new" in url:
				new_pubkey = utils.getargv("new", "", 1, url)
				try:
					new_keys = PublicKey(new_pubkey)
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_pubkey"}, resp_format)
					continue
				try:
					message = libnacl.sign.Verifier(new_keys.hex_pk()).verify(content) # Verify
				except ValueError:
					sendResponse(client, "401 Unauthorized", {"error": "bad_signature"}, resp_format)
					continue
				try:
					message = self.keys.decrypt_seal(message) # Decrypt
				except libnacl.CryptError:
					sendResponse(client, "403 Forbidden", {"error": "bad_encryption"}, resp_format)
					continue
				try:
					message = ubjson.loadb(message)
				except ubjson.decoder.DecoderException:
					sendResponse(client, "400 Bad Request", {"error": "bad_ubjson"}, resp_format)
					continue
				
				if new_pubkey == message["pubkey"]:
					if not new_pubkey in self.peers_index:
						peer = utils.Peer(new_pubkey, message["host"], message["port"], True)
						self.peers.append(peer)
						self.peers_index[new_pubkey] = peer
						utils.logPrint("Add "+str(peer), utils.LOG_TRACE)
					else:
						self.peers_index[new_pubkey].up = True
						utils.logPrint("Up "+str(peer), utils.LOG_TRACE)
			
			# Send response
			sendResponse(client, "200 OK", resp, resp_format)
		self.sock.close()
	
	def stop(self):
		self.work = False
		self.sock.shutdown(socket.SHUT_WR)

class ClientThread(Thread):
	def __init__(self, conf, peers, peers_index, keys, pool, tx_in_index, tx_out_index, db_txs):
		Thread.__init__(self)
		
		self.conf = conf
		self.peers = peers
		self.peers_index = peers_index
		self.keys = keys
		self.pool = pool
		self.tx_in_index = tx_in_index
		self.tx_out_index = tx_out_index
		self.db_txs = db_txs
		
		self.bma_endpoints = ["BMAS "+host for host in conf["client"]["bma_hosts"]]
		self.work = True
	
	def detectPeers(self):# Check known peers and ask them for their known peer list
		utils.logPrint("Start peers detection", utils.LOG_TRACE)
		modified = True
		asked = []
		
		while modified:
			modified = False
			
			for peer in self.peers:
				if peer.pubkey in asked:
					continue
				asked.append(peer.pubkey)
				
				message = ubjson.dumpb({
					"pubkey": self.keys.pubkey,
					"host": self.conf["server"]["public_host"],
					"port": self.conf["server"]["public_port"],
					"time": time.time()
				})
				message = peer.keys.encrypt_seal(message) # Encrypt
				message = self.keys.sign(message) # Sign
				utils.logPrint("Ask "+str(peer), utils.LOG_TRACE)
				try:
					header, content = utils.sdata((peer.host, peer.port), "POST", "/list/new/"+self.keys.pubkey, message, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"]) # Send
				except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
					peer.up = False
					utils.logPrint("Down "+str(peer), utils.LOG_TRACE)
					continue
				
				peer.up = True
				utils.logPrint("Up "+str(peer), utils.LOG_TRACE)
				try:
					message = ubjson.loadb(content)
					assert "peers" in message
					assert type(message["peers"]) == list
				except (ubjson.decoder.DecoderException, AssertionError):
					utils.logPrint("Bad json from "+str(peer), utils.LOG_ERROR)
					continue
				for i_peer in message["peers"]:
					try:
						assert "pubkey" in i_peer and "host" in i_peer and "port" in i_peer
						int(i_peer["port"])
					except (AssertionError, ValueError):
						utils.logPrint("Bad json from "+str(peer), utils.LOG_ERROR)
						continue
					if i_peer["pubkey"] in self.peers_index or i_peer["pubkey"] == self.keys.pubkey:
						continue
					try:
						new_peer = utils.Peer(i_peer["pubkey"], i_peer["host"], i_peer["port"], None)
					except ValueError:
						utils.logPrint("Bad pubkey from "+str(peer), utils.LOG_ERROR)
						continue
					self.peers.append(new_peer)
					self.peers_index[new_peer.pubkey] = new_peer
					modified = True
					utils.logPrint("Add "+str(new_peer), utils.LOG_TRACE)
		utils.logPrint("Finished peers detection", utils.LOG_TRACE)
	
	async def mix(self):
		can_mix = False
		for tx in self.pool:
			if not tx.need_send and not tx.need_confirm and not tx.tx_sent:
				can_mix = True
				break
		if not can_mix:
			return
			
		utils.logPrint("Starting mix", utils.LOG_TRACE)
		t = time.time()
		client = None
		for bma_endpoint in self.bma_endpoints:
			client = Client(bma_endpoint)
			try:
				await client(bma.node.summary)
				break
			except:
				utils.logPrint("BMA down: "+bma_endpoint, utils.LOG_WARN)
				await client.close()
				client = None
		if client:
			utils.logPrint("BMA up: "+bma_endpoint, utils.LOG_TRACE)
			ready_txs = []
			
			# Get txs history
			history = await client(bma.tx.times, self.keys.pubkey, int(t-self.conf["mix"]["mix_req_age_max"]), int(t))
			await client.close()
			
			for tx in self.pool:
				if tx.tx_sent:
					continue
				if tx.need_send or tx.need_confirm:
					continue
				wanted_output = str(tx.in_amount) +":"+ str(tx.in_base) +":SIG("+ self.keys.pubkey +")"
				ok = False
				for in_tx in history["history"]["received"]:
					if (not "output" in in_tx and not "outputs" in in_tx) or not "comment" in in_tx:
						continue
					if in_tx["comment"] == utils.gen_comment(tx.in_seeds) \
					   and wanted_output in in_tx["output" if "output" in in_tx else "outputs"]:
						ok = True
						break
				if ok:
					ready_txs.append(tx)
			
			# Group txs by amount
			ready_txs_by_amount = {}
			for tx in ready_txs:
				if not tx.in_amount in ready_txs_by_amount:
					ready_txs_by_amount[tx.in_amount] = []
				ready_txs_by_amount[tx.in_amount].append(tx)
			
			# Check and send txs
			for amount in ready_txs_by_amount:
				txs = ready_txs_by_amount[amount]
				if len(txs) >= self.conf["mix"]["mix_min_txs"]:
					random.shuffle(txs)
					for tx in txs:
						try:
							utils.sendTransaction(self.keys, tx.receiver_pubkey, tx.out_amount, utils.gen_comment(tx.out_seeds))
							tx.tx_sent = True
							tx.export_ubjson(self.db_txs)
						except socket.timeout:
							utils.logPrint("Error when sending tx: timeout", utils.LOG_ERROR)
						except Exception as e:
							utils.logPrint("Error when sending tx: " + str(e), utils.LOG_ERROR)
				else:
					utils.logPrint("Not enough ready txs to mix ("+str(amount)+"u) (only "+str(len(txs))+")", utils.LOG_TRACE)
			
			utils.logPrint("Mix finished", utils.LOG_TRACE)
		else:
			utils.logPrint("All BMA endpoints down: cannot mix!", utils.LOG_ERROR)
	
	def run(self):
		asyncio.new_event_loop().run_until_complete(self.startClient())
	
	async def startClient(self):
		next_peers_detection = 0
		next_mix = time.time() + self.conf["mix"]["mix_interval"]
		
		while self.work:
			t = time.time()
			
			# Detect peers
			if t > next_peers_detection:
				self.detectPeers()
				next_peers_detection = time.time()+120
			
			# Mix
			if t > next_mix:
				await self.mix()
				next_mix = time.time() + self.conf["mix"]["mix_interval"]
			
			for tx in self.pool:
				
				if tx.need_send:
					utils.logPrint("Send "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
					if tx.receiver_pubkey in self.peers_index:
						peer = self.peers_index[tx.receiver_pubkey]
						message = self.keys.sign(tx.out_seeds[1] + tx.message) # Sign
						
						try:
							header, content = utils.sdata((peer.host, peer.port), "POST", "/mix/"+self.keys.pubkey+"/"+str(tx.out_amount)+"/"+str(tx.out_base), message, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
							data = ubjson.loadb(content)
							assert data["mix_ok"] == tx.out_seeds[1]
							tx.need_send = False
							tx.export_ubjson(self.db_txs)
							peer.up = True
							utils.logPrint("Sent "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
							utils.logPrint("Up "+str(peer), utils.LOG_TRACE)
						except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
							peer.up = False
							utils.logPrint("Down "+str(peer), utils.LOG_TRACE)
						except (ubjson.decoder.DecoderException, KeyError, AssertionError):
							utils.logPrint("Error: bad response from "+str(peer), utils.LOG_WARN)
					
					else:
						utils.logPrint("Unknown peer: "+tx.receiver_pubkey, utils.LOG_WARN)
				
				elif tx.can_confirm and tx.need_confirm and tx.send_confirm:
					utils.logPrint("Confirm "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
					if tx.sender_pubkey in self.peers_index:
						peer = self.peers_index[tx.sender_pubkey]
						message = tx.genMixConfirm(self.keys)
						
						try:
							header, content = utils.sdata((peer.host, peer.port), "POST", "/confirm/"+self.keys.pubkey+"/"+tx.in_seeds[1].hex(), message, proxy=self.conf["client"]["proxy"], proxy_onion_only=self.conf["client"]["proxy_onion_only"])
							data = ubjson.loadb(content)
							assert data["confirm_ok"] == tx.in_seeds[2]
							tx.need_confirm = False
							tx.export_ubjson(self.db_txs)
							peer.up = True
							utils.logPrint("Confirmed "+tx.sender_pubkey[:8]+" -> "+tx.receiver_pubkey[:8]+" = "+str(tx.in_amount)+":"+str(tx.in_base)+" -> "+str(tx.out_amount)+":"+str(tx.out_base), utils.LOG_TRACE)
							utils.logPrint("Up "+str(peer), utils.LOG_TRACE)
						except (ConnectionRefusedError, socks.GeneralProxyError, socket.gaierror, socket.timeout):
							peer.up = False
							utils.logPrint("Down "+str(peer), utils.LOG_TRACE)
						except (ubjson.decoder.DecoderException, KeyError, AssertionError):
							utils.logPrint("Error: bad response from "+str(peer), utils.LOG_WARN)
					
					else:
						utils.logPrint("Unknown peer: "+tx.sender_pubkey, utils.LOG_WARN)
			
			# Remove expired requests
			expire_txs = []
			expire_t = t - self.conf["mix"]["mix_req_age_max"]
			for tx in self.pool:
				if tx.date < expire_t:
					expire_txs.append(tx)
			for tx in expire_txs:
				self.tx_in_index.pop(tx.in_seeds[1])
				self.tx_out_index.pop(tx.out_seeds[1])
				self.db_txs.delete(tx.in_seeds[2])
				self.pool.remove(tx)
			if len(expire_txs) > 0:
				utils.logPrint("Removed "+str(len(expire_txs))+" expired txs", utils.LOG_TRACE)
			
			time.sleep(5)

def getCredentials(conf):
	salt = conf["crypto"]["id_salt"]
	if salt == "":
		salt = getpass.getpass("Enter your passphrase (salt): ")
	password = conf["crypto"]["id_password"]
	if password == "":
		password = getpass.getpass("Enter your password: ")
	return salt, password

# Main function
def main():
	# Load conf & peers
	conf = readConfig(DIR)
	peers, peers_index = utils.readPeers(DIR)
	
	# Load txs
	pool = []
	tx_in_index = {}
	tx_out_index = {}
	db_txs = plyvel.DB(DIR+"/db_txs", create_if_missing=True)
	load_txs(db_txs, pool, tx_in_index, tx_out_index)
	
	# Get private key
	salt, password = getCredentials(conf)
	keys = SigningKey.from_credentials(salt, password)
	utils.logPrint("Pubkey: "+keys.pubkey, utils.LOG_INFO)
	
	# Start threads
	clientThread = ClientThread(conf, peers, peers_index, keys, pool, tx_in_index, tx_out_index, db_txs)
	serverThread = ServerThread(conf, peers, peers_index, keys, pool, tx_in_index, tx_out_index, db_txs)
	
	clientThread.start()
	serverThread.start()
	
	# Wait
	try:
		while True:
			input()
	except KeyboardInterrupt:
		utils.logPrint("Stopping (^C)...", utils.LOG_INFO)
	
	# Stop threads
	serverThread.stop()
	clientThread.work = False
	serverThread.join()
	clientThread.join()
	
	# Save
	utils.writePeers(DIR, peers)
	save_txs(db_txs, pool)
	db_txs.close()

if __name__ == "__main__":
	
	if "-v" in sys.argv:
		utils.VERBOSITY |= utils.LOG_TRACE
	
	DIR = os.path.expanduser(utils.getargv("-d", DIR))
	if DIR != "" and DIR[len(DIR)-1] == "/":
		DIR = DIR[:len(DIR)-1] # Remove last slash
	os.makedirs(DIR, exist_ok=True)
	
	conf_overwrite = {}
	if "-P" in sys.argv:
		import subprocess
		print("Fetching public address...")
		PUBLIC_HOST = subprocess.run(['curl', '-4', 'https://zettascript.org/tux/ip/'], stdout=subprocess.PIPE).stdout.decode("utf-8")
		print("Public host: " + PUBLIC_HOST)
		conf_overwrite["server.public_host"] = PUBLIC_HOST
		readConfig(DIR, conf_overwrite)
	
	if "-s" in sys.argv:
		main()
	
	elif "-i" in sys.argv:
		conf = readConfig(DIR)
		utils.readPeers(DIR)
	
	elif "-I" in sys.argv:
		ID_SALT, ID_PASSWORD = gen_keys()
		conf = readConfig(DIR)
		utils.readPeers(DIR)
	
	elif "-k" in sys.argv:
		conf = readConfig(DIR)
		salt, password = getCredentials(conf)
		keys = SigningKey.from_credentials(salt, password)
		print(keys.pubkey)
	
	elif "-V" in sys.argv or "--version" in sys.argv:
		print(VERSION)
	
	elif "--help" in sys.argv:
		print("\
ĞMixer-py Server "+VERSION+"\n\
\n\
Options:\n\
 -c <path>  Change config & data dir\n\
  default: ~/.gmixer\n\
 -s         Start server\n\
 -i         Init config\n\
 -I         Init config (generate random keys)\n\
 -P         Auto set public address (overwrites config)\n\
 -k         Display public key\n\
 -v         Verbose\n\
 -V         Display version\n\
 --version\n\
 --help     Display help\n\
")
