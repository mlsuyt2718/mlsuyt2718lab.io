#!/usr/bin/env python3

"""
	ĞMixer-py Test Client (only for testing server)
	This file is part of ĞMixer-py.

	ĞMixer-py is free software: you can redistribute it and/or modify
	it under the terms of the GNU Affero General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
	(at your option) any later version.

	ĞMixer-py is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU Affero General Public License for more details.

	You should have received a copy of the GNU Affero General Public License
	along with ĞMixer-py.  If not, see <https://www.gnu.org/licenses/>.
	
Sources:
https://www.ietf.org/rfc/rfc3092.txt <- Very important RFC, please read it
"""

import asyncio, getpass, random
import ubjson
import plyvel
import libnacl.sign
from utils import *

VERSION = "0.1.0"
AUTHORS = ["Pascal Engélibert <tuxmain@zettascript.org>"]

DIR = "~/.gmixer"

class Confirmation():
	def __init__(self, client_pubkey, node_pubkey, raw):
		self.client_pubkey = client_pubkey
		self.node_pubkey = node_pubkey
		self.raw = raw
		
		data = libnacl.sign.Verifier(PublicKey(node_pubkey).hex_pk()).verify(raw)
		data = ubjson.loadb(data)
		print(data)
		assert data["document"] == "gmixer-mixconfirm1"
		self.sender_pubkey = data["sender_pubkey"]
		self.in_seeds = data["in_seeds"]
		self.in_amount = data["in_amount"]
		self.in_base = data["in_base"]
		self.receiver_pubkey = data["receiver_pubkey"]
		self.out_seeds = data["out_seeds"]
		self.out_amount = data["out_amount"]
		self.out_base = data["out_base"]
	
	def to_dict(self):
		return {
			"client_pubkey": self.client_pubkey,
			"node_pubkey": self.node_pubkey,
			"raw": self.raw,
			"sender_pubkey": self.sender_pubkey,
			"in_amount": self.in_amount,
			"in_base": self.in_base,
			"receiver_pubkey": self.receiver_pubkey,
			"out_amount": self.out_amount,
			"out_base": self.out_base
		}

def get_peers(host, proxy=None, proxy_onion_only=False):
	header, content = sdata(host, "GET", "/pubkey/list", proxy=proxy, proxy_onion_only=proxy_onion_only)
	
	try:
		data = ubjson.loadb(content)
		assert "pubkey" in data and "peers" in data
		assert type(data["pubkey"]) == str and type(data["peers"]) == list
	except (ubjson.decoder.DecoderException, AssertionError):
		print("Error: bad UBJSON")
		return
	
	peers = [Peer(data["pubkey"], host[0], host[1], True)]
	for peer in data["peers"]:
		peers.append(Peer(peer["pubkey"], peer["host"], peer["port"], peer["up"]))
	
	return peers

def build_path(peers, receiver, layers=3):
	up_peers = []
	for peer in peers:
		if peer.up:
			up_peers.append(peer)
	if len(up_peers) < layers:
		return None, None
	path = []
	host = None
	peer = None
	for i in range(layers):
		while True:
			peer = random.choice(up_peers)
			if not peer.pubkey in path:
				break
		path.append(peer.pubkey)
		if host == None:
			host = (peer.host, peer.port)
	path.append(receiver)
	return host, path

def mix(db_txs, amount, base, sender, path, host, proxy=None, proxy_onion_only=False, send_tx=True):
	start_time = time.time()
	
	onetime_keys = []
	comment_seeds = [[secrets.token_bytes(32), None, secrets.token_bytes(32)]] # [client, sender, receiver]
	message = b""
	i = len(path)-2
	while i >= 0:
		ot_salt, ot_password = gen_keys()
		ot_keys = SigningKey.from_credentials(ot_salt, ot_password)
		onetime_keys.insert(0, ot_keys)
		out_seeds = comment_seeds[0]
		in_seeds = [secrets.token_bytes(32), None, None]
		comment_seeds.insert(0, in_seeds)
		message = PublicKey(path[i]).encrypt_seal(ubjson.dumpb({
			"receiver": path[i+1],
			"onetime": ot_keys.pubkey,
			"in_seeds": in_seeds,
			"out_seeds": out_seeds,
			"message": message
		}))
		i -= 1
	
	comment_seeds[0][1] = secrets.token_bytes(32)
	message = sender.sign(comment_seeds[0][1] + message)
	
	while True:
		try:
			header, content = sdata(host, "POST", "/pubkey/mix/"+sender.pubkey+"/"+str(int(amount))+"/"+str(int(base))+"/client", message, proxy=proxy, proxy_onion_only=proxy_onion_only)
		except ConnectionRefusedError:
			print("Error: Connection refused; retrying...")
			time.sleep(3)
			continue
		try:
			data = ubjson.loadb(content)
			assert "mix_ok" in data and data["mix_ok"] == comment_seeds[0][1]
		except (ubjson.decoder.DecoderException, AssertionError):
			print(content)
			print("Error: bad response; retrying...")
			time.sleep(3)
			continue
		break
	
	print("Please wait; nodes are working...")
	time.sleep(5)
	while True:
		time.sleep(5)
		print("Asking input node for confirmation...")
		try:
			header, content = sdata(host, "GET", "/getconfirm/"+sender.pubkey+"/"+comment_seeds[0][1].hex(), proxy=proxy, proxy_onion_only=proxy_onion_only)
		except ConnectionRefusedError:
			continue
		
		# Check confirmation existence
		try:
			message = ubjson.loadb(content)
		except ubjson.decoder.DecoderException:
			print("Error: bad UBJSON")
			continue
		
		if "confirm" in message:
			print("This is a confirmation!")
			data = message["confirm"]
			
			try:
				data = libnacl.sign.Verifier(PublicKey(path[0]).hex_pk()).verify(data) # Verify
			except ValueError:
				print("Bad signature")
				continue
			
			try:
				data = sender.decrypt_seal(data) # Decrypt
			except libnacl.CryptError:
				print("Bad encryption")
				continue
			
			if comment_seeds[0][1] != data[:32]:
				print("Bad seed0_1")
				continue
			
			raw_confirms = []
			offset = 64
			while offset+4 < len(data):
				length = bin_to_int(data[offset:offset+4])
				offset += 4
				raw_confirms.append(data[offset:offset+length])
				offset += length
			
			if len(raw_confirms) != len(path)-1:
				print("Bad number of confirmations ("+str(len(raw_confirms))+")")
			confirms = []
			i = 0
			for raw in raw_confirms:
				raw = onetime_keys[i].decrypt_seal(raw)
				try:
					confirm = Confirmation(sender.pubkey, path[i], raw)
				except:
					print("Bad confirmation #"+str(i))
					return
				if confirm.in_amount != int(amount) or confirm.in_base != int(base):
					print("Bad in_amount or in_base #"+str(i))
					return
				if confirm.out_amount != int(amount) or confirm.out_base != int(base):
					print("Bad out_amount or out_base #"+str(i))
					return
				if (i == 0 and confirm.sender_pubkey != sender.pubkey) or (i > 0 and confirm.sender_pubkey != path[i-1]):
					print("Bad sender_pubkey #"+str(i))
					return
				if confirm.receiver_pubkey != path[i+1]:
					print("Bad receiver_pubkey #"+str(i))
					return
				
				for k in range(3):
					if comment_seeds[i][k] != None:
						if comment_seeds[i][k] != confirm.in_seeds[k]:
							print("Bad in_seeds #"+str(i)+"_"+str(k))
							return
					elif type(confirm.in_seeds[k]) == bytes and len(confirm.in_seeds[k]) == 32:
						comment_seeds[i][k] = confirm.in_seeds[k]
					else:
						print("Bad in_seeds #"+str(i)+"_"+str(k))
				for k in range(3):
					if comment_seeds[i+1][k] != None:
						if comment_seeds[i+1][k] != confirm.out_seeds[k]:
							print("Bad out_seeds #"+str(i)+"_"+str(k))
							return
					elif type(confirm.out_seeds[k]) == bytes and len(confirm.out_seeds[k]) == 32:
						comment_seeds[i+1][k] = confirm.out_seeds[k]
					else:
						print("Bad out_seeds #"+str(i)+"_"+str(k))
				
				confirms.append(confirm)
				i += 1
			
			print("OK, total duration = "+str(round(time.time()-start_time, 2))+"s")
			if not send_tx:
				print("Remind: no-tx mode")
			
			if input("OK? [yn]: ").lower() == "y":
				message = {
					"sender": sender.pubkey,
					"path": path,
					"amount": amount,
					"onetime_keys": [ot_keys.hex_sk().decode() for ot_keys in onetime_keys],
					"confirms": [confirm.to_dict() for confirm in confirms],
					"sent": False
				}
				db_txs.put(comment_seeds[0][1], PublicKey(sender.pubkey).encrypt_seal(ubjson.dumpb(message)))
				
				if send_tx:
					try:
						sendTransaction(sender, path[0], amount, gen_comment(comment_seeds[0]))
						
						message["sent"] = True
						db_txs.put(comment_seeds[0][1], PublicKey(sender.pubkey).encrypt_seal(ubjson.dumpb(message)))
					except socket.timeout:
						logPrint("Error when sending tx: timeout", LOG_ERROR)
					except Exception as e:
						logPrint("Error when sending tx: " + str(e), LOG_ERROR)
				return

async def test1(db_txs, host, receiver, amount=1000, layers=3, proxy=None, proxy_onion_only=False, send_tx=True):
	if amount < 100:
		print("!! Warning !!\nYou are going to send less than 1.00, all this money will be destroyed by Duniter.\nPlease always send 1.00 or more.")
		if input("Do it anyway? [yn]: ").lower() != "y":
			return
	
	print("IDs of the expeditor account:")
	salt = getpass.getpass("Salt: ")
	password = getpass.getpass("Psw: ")
	keys = SigningKey.from_credentials(salt, password) # sender
	print(keys.pubkey)
	if input("Is that the right pubkey? [yn]: ").lower() != "y":
		return
	
	peers = get_peers(host, proxy, proxy_onion_only)
	if peers == None:
		print("Error getting peer list")
		exit(1)
	while True:
		host1, path = build_path(peers, receiver, layers)
		if path == None:
			print("Error: not enough peers")
			exit(1)
		for peer in path:
			print(peer)
		if input("OK? [yn]: ").lower() == "y":
			break
	
	mix(db_txs, amount, 0, keys, path, host1, proxy, proxy_onion_only, send_tx)

if __name__ == "__main__":
	if "--help" in sys.argv:
		print("ĞMixer-py client "+VERSION+"""

Options:
 -r <pubkey>       receiver pubkey
 -l <int>          number of onion layers (default 3)
 -a <int>          amount (default 1000 = 10.00Ğ1)
 -h <host> <port>  host for sync peer list
 -p <host> <port>  SOCKS5 proxy
 --onion           use proxy only when connecting to .onion
 -d <path>         config directory (default ~/.gmixer)
 --no-tx           Do not send transaction (for debug)

Example:
python3 client.py -h svetsae7j3usrycn.onion 10951 -r 78ZwwgpgdH5uLZLbThUQH7LKwPgjMunYfLiCfUCySkM8 -p 127.0.0.1 9050 --onion
 ⤷	send 10Ğ1 to Duniter developers
""")
		exit()
	
	DIR = os.path.expanduser(getargv("-d", DIR))
	if DIR != "" and DIR[len(DIR)-1] == "/":
		DIR = DIR[:len(DIR)-1] # Remove last slash
	os.makedirs(DIR, exist_ok=True)
	
	receiver = getargv("-r", "")
	if receiver == "":
		print("Error: No receiver set (try option --help)")
		exit(1)
	host = getargv("-h", "")
	if host == "":
		print("Error: No host set (try option --help)")
		exit(1)
	port = int(getargv("-h", "", 2))
	amount = int(getargv("-a", "1000"))
	layers = int(getargv("-l", "3"))
	proxy = None
	if "-p" in sys.argv:
		proxy = (getargv("-p", "127.0.0.1", 1), int(getargv("-p", 9050, 2)))
	proxy_onion_only = "--onion" in sys.argv
	send_tx = not "--no-tx" in sys.argv
	
	db_txs = plyvel.DB(DIR+"/client_db_txs", create_if_missing=True)
	
	asyncio.get_event_loop().run_until_complete(test1(db_txs, (host, port), receiver, amount, layers, proxy, proxy_onion_only, send_tx))
